package;
import flash.sampler.NewObjectSample;
import flash.utils.ByteArray;
import flash.system.MessageChannel;

/**
 * ...
 * @author Tom
 */
interface MessageReceiver {

	public function getReceivedObject(chan:MessageChannel, changer:Dynamic):Message;
	
}

class ScreenInfoMessageReceiver implements MessageReceiver {
	
	public function new(){
		
	}
	
	public function getReceivedObject(chan:MessageChannel, changer:Dynamic) {
		var c:Dynamic = chan.receive();
		changer(new ChunkUpdateReceiver());
		return new ScreenInfoMessage(c.screenWidth, c.screenHeight, c.chunkWidth, c.chunkHeight);
	}
}

class ChunkUpdateReceiver implements MessageReceiver {
	
	public function new(){
		
	}
	
	public function getReceivedObject(chan:MessageChannel, changer:Dynamic) {
		var c:Dynamic = chan.receive();
		return new ChunkUpdateMessage(c.chunkX,c.chunkY,c.chunkData);
	}
	
}

interface Message {
	public function handle(data:Dynamic):Void;
}

class ChunkUpdateMessage implements Message {
	
	public function handle(data:Dynamic) {
		cast(data, Display).updateChunk(this.chunkX, this.chunkY, this.chunkData);
	}
	
	private var chunkX:Int;
	private var chunkY:Int;
	private var chunkData:ByteArray;
	
	public function new(chunkX:Int, chunkY:Int, chunkData:ByteArray) {
		this.chunkX = chunkX;
		this.chunkY = chunkY;
		this.chunkData = chunkData;
	}
	
	public function getChunkX() {
		return this.chunkX;
	}
	
	public function getChunkY() {
		return this.chunkY;
	}
	
	public function getChunkdata() {
		return this.chunkData;
	}
}

class ScreenInfoMessage implements Message {
	
	public function handle(data:Dynamic) {
		cast(data, Display).setUp(this.screenWidth, this.screenHeight, this.chunkWidth, this.chunkHeight);
	}
	
	private var screenWidth:Int;
	private var screenHeight:Int;
	private var chunkWidth:Int;
	private var chunkHeight:Int;
	
	
	public function new(screenWidth:Int, screenHeight:Int, chunkWidth:Int, chunkHeight:Int) {
		this.screenWidth = screenWidth;
		this.screenHeight = screenHeight;
		this.chunkWidth = chunkWidth;
		this.chunkHeight = chunkHeight;
	}
	
	public function getChunkWidth(){
		return this.chunkWidth;
	}
	
	public function getChunkHeight(){
		return this.chunkHeight;
	}
	
	
	public function getScreenWidth() {
		return this.screenWidth;
	}
	
	public function getScreenHeight() {
		return this.screenHeight;
	}
	
}