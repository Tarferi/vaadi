package;

import Chunk;
import flash.utils.ByteArray;
import ru.stablex.ui.widgets.Widget;

/**
 * ...
 * @author Tom
 */
class Display extends Widget {
	
	private var data:Dynamic;

	private var maxX:Int;
	private var maxY:Int;
	
	private var qwidth:Int;
	private var qheight:Int;
	
	private var chunkX:Int;
	private var chunkY:Int;
	
	
	public function new() {
		super();
	}
	
	public function updateChunk(chunkX:Int, chunkY:Int, chunkData:ByteArray) {
		this.data[chunkY][chunkX].updateInnerData(chunkData);
	}
	
	public function setUp(width: Int, height:Int, chunkX:Int, chunkY:Int) {
		super.left = 0;
		super.top = 0;
		this.qwidth = width;
		this.qheight = height;
		this.chunkX = chunkX;
		this.chunkY = chunkY;
		this.data = new Array( );
		this.maxX = Math.ceil(width / chunkX);
		this.maxY = Math.ceil(height / chunkY);
		for (y in 0...this.maxY) {
			var arr:Dynamic= new Array();
			for (x in 0...this.maxX) {
				arr[x] = new Chunk( x, y, chunkX, chunkY);
				addChild(arr[x]);
			}
			this.data[y] = arr;
		}
		this.scaleX = 0.5;
		this.scaleY = 0.5;
	}
}