package;

import flash.display.Sprite;
import flash.display.StageAlign;
import flash.display.StageScaleMode;
import flash.Lib;
import Connector;
import Chunk;
import Display;
import flash.net.Socket;
import ru.stablex.ui.UIBuilder;
import flash.system.ApplicationDomain;

import flash.display.LoaderInfo;
import flash.system.Worker;
import flash.system.WorkerDomain;
import flash.utils.ByteArray;
import flash.system.MessageChannel;
import Message;
import flash.events.Event;

/**
 * ...
 * @author Tom
 */
class Main extends Sprite {
	
	private var display:Display=new Display();
	
	private var connector:Connector;
	private var receiver:MessageReceiver;
	
	private function connectorThread() {
		this.connector = new Connector("45.33.117.213", 19467);
		//this.connector = new Connector("127.0.0.1", 9008);
		this.connector.connect();
	}
	
	private function new() {
		if(Worker.current.isPrimordial) {
			super();
			Lib.current.addChild(this.display);
			this.receiver = new ScreenInfoMessageReceiver();
		} else {
			var type:Int = Worker.current.getSharedProperty("type");
			switch(type) {
				case 0:
					this.connectorThread();
			}
		}
	}
	
	private function getWorker(creationArray:ByteArray, type:Int):Worker {
		var w:Worker = WorkerDomain.current.createWorker(creationArray);
		w.setSharedProperty("type", type);
		return w;
	}
	
	private function startThreads() {
		if(Worker.current.isPrimordial) {
			var w:Worker = this.getWorker(Lib.current.loaderInfo.bytes, 0);
			
			var sendChannel:MessageChannel;
			//sendChannel = Worker.current.createMessageChannel(w);
			sendChannel = w.createMessageChannel(Worker.current);
			w.setSharedProperty("incomingChannel", sendChannel);
			
			sendChannel.addEventListener(Event.CHANNEL_MESSAGE, function(event:Event){
				var obj:Message = this.receiver.getReceivedObject(sendChannel, this.setReceiver);
				//trace("Received: " + obj);
				obj.handle(this.display);
			});
				
			w.start();
		}
	}
	
	public function setReceiver(rec:MessageReceiver) {
		this.receiver = rec;
	}
	

	
    static function main() {
		UIBuilder.init();
		var main:Main = new Main();
		main.startThreads();
		
    } 
	
}