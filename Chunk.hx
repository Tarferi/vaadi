package;
import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.geom.Rectangle;
import flash.utils.ByteArray;
import ru.stablex.ui.UIBuilder;
import flash.geom.Matrix;
import ru.stablex.ui.widgets.Widget;


/**
 * ...
 * @author Tom
 */

class Chunk extends Widget {
	
	private var data:ByteArray;
		
	private var qwidth:Int;
	private var qheight:Int;
	
	private var offX:Int;
	private var offY:Int;
	
	private var bitmap:Bitmap;
	private var rect:Rectangle;

	
	private function getEmptyBitMap() {
		var tmp = new ByteArray();
		for (y in 0...this.qwidth) {
			for (x in 0...this.qheight) {
				tmp.writeUnsignedInt(0xff0000);
			}
		}
		tmp.position = 0;
		return tmp;
	}
	
	public function new(offX:Int, offY:Int, width: Int, height:Int) {
		super();
		super.left = offX * width;
		super.top = offY * height;
		this.qwidth = width;
		this.qheight = height;
		var bitmapData:BitmapData = new BitmapData(this.qwidth, this.qheight, false, 0);
		this.bitmap = new Bitmap(bitmapData);
		this.rect = new Rectangle(0, 0, this.qwidth, this.qheight);
		this.data = this.getEmptyBitMap();
		this.totalSize = width * height;
		this.updateInnerData(this.data);
	}
	
	private var totalSize:Int;
	
	public function updateInnerData(data: ByteArray) {
		data.position = 0;
		/*
		this.data.position = 0;
		this.data.writeBytes(data, 0, data.bytesAvailable);
		this.data.position = 0;
		*/
		this.bitmap.bitmapData.setPixels(this.rect, data);
		this.refresh();
	}

    override public function refresh () : Void {
        super.refresh();

		var matrix:Matrix = new Matrix();
		matrix.tx = 0;
		matrix.ty = 0;
		
	
		graphics.beginBitmapFill(this.bitmap.bitmapData, matrix);
		graphics.drawRect(0, 0, this.qwidth, this.qheight);
		graphics.endFill();
		/*
		graphics.beginFill(0);
		graphics.moveTo(0, 0);
		graphics.lineTo(this.qwidth,0);
		graphics.lineTo(0, this.qheight);
		graphics.endFill();
		*/
    }
}