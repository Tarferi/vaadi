package;

import flash.events.Event;
import flash.net.Socket;
import flash.system.Security;
import flash.events.IOErrorEvent;
import flash.events.SecurityErrorEvent;
import flash.events.ProgressEvent;
import flash.utils.ByteArray;
import flash.system.MessageChannel;
import flash.system.Worker;
import flash.utils.CompressionAlgorithm;
import Message;

/**
 * ...
 * @author Tom
 */
class Connector implements DisplayUpdater {
	
	private var ip:String;
	private var port:Int;

	private var channel:MessageChannel;

	private var sock:Socket;

	public function new(ip:String, port:Int) {
		this.ip = ip;
		this.port = port;
		Security.allowDomain(ip);
		Security.loadPolicyFile(ip+":"+port);
		this.channel = Worker.current.getSharedProperty("incomingChannel");
		
		this.readerChanger = new ReaderChanger(
		function(newReader:SocketReader) {
			this.currentReader = newReader;
		}, function(screenWidth,screenHeight, chunkWidth, chunkHeight){
			this.screenSizeX = screenWidth;
			this.screenSizeY = screenHeight;
			this.chunkSizeX = chunkWidth;
			this.chunkSizeY = chunkHeight;
			this.channel.send(new ScreenInfoMessage(screenWidth, screenHeight, chunkWidth, chunkHeight));
		}
		);
		
	}
	

	private var screenSizeX:Int;
	private var screenSizeY:Int;
	
	private var chunkSizeX:Int;
	private var chunkSizeY:Int;
		
	private var availableBytes = 0;
	private var requestBytes = 0;
	
	private var currentReader:SocketReader = new FirstXMLReader();
	private var readerChanger:ReaderChanger;
	
	public function connect() {
		try {
			this.sock= new Socket();
			
			this.sock.addEventListener(IOErrorEvent.IO_ERROR, function(event:IOErrorEvent) {
				trace("IO Event");
			});
			this.sock.addEventListener(SecurityErrorEvent.SECURITY_ERROR, function(event:SecurityErrorEvent) {
				trace("Security event");
			});
			this.sock.addEventListener(Event.CLOSE, function(event:Event) {
				trace("Socket closed");
			});
			this.sock.addEventListener(Event.CONNECT, function(event:Event) {
				//trace("Socket opened");
			});
			this.sock.addEventListener(ProgressEvent.SOCKET_DATA, function(event:ProgressEvent) {
				this.availableBytes += cast(event.bytesLoaded, Int);
				this.availableBytes = this.currentReader.newDataAvailable(this.availableBytes, this, this.sock, this.readerChanger);
			});
			this.sock.connect(this.ip, this.port);
			//trace("Connecting...");
		} catch (e:Dynamic) {
			trace("Error: " + e);
		}
	}
	
	public function updateChunk(chunkX:Int, chunkY:Int, data:ByteArray):Void {
		//trace("Updating chunk at " + chunkX + ":" + chunkY);
		this.channel.send(new ChunkUpdateMessage(chunkX, chunkY, data));
	}
}


interface SocketReader {
	
	public function newDataAvailable(totalAvailable:Int,displayUpdater:DisplayUpdater, sock:Socket, rc:ReaderChanger):Int;
	
}

interface DisplayUpdater {
	
	public function updateChunk(chunkX:Int, chunkY:Int, data:ByteArray):Void;
	
}

class ReaderChanger {
	
	private var setter:Dynamic;
	private var screenSetter:Dynamic;
	
	public function new(setter:Dynamic,screenSetter:Dynamic) {
		this.setter = setter;
		this.screenSetter = screenSetter;
	}
	
	public function setReader(reader:SocketReader) {
		this.setter(reader);
	}
	
	public function setScreenData(screenWidth:Int, screenHeight:Int, chunkWidth:Int, chunkHeight:Int) {
		this.screenSetter(screenWidth, screenHeight, chunkWidth, chunkHeight);
	}
}

class FirstXMLReader implements SocketReader {
	
	public function new() {
		
	}
	
	public function newDataAvailable(totalAvailable:Int, displayUpdater:DisplayUpdater, sock:Socket, rc:ReaderChanger):Int {
		while (totalAvailable > 0) {
			var c:Int = sock.readByte();
			if (c == 0) {
				rc.setReader(new SecondDisplayReader());
				break;
			} else {
				totalAvailable--;
			}
		}
		return totalAvailable;
	}
}

class SecondDisplayReader implements SocketReader {
	
	public function new() {
		
	}
	
	public function newDataAvailable(totalAvailable:Int, displayUpdater:DisplayUpdater, sock:Socket, rc:ReaderChanger):Int {
		if (totalAvailable >= 2) {
			var p1:Int = sock.readByte()&0xff;
			var p2:Int = sock.readByte()&0xff;
			var chunkSizeX:Int = p1 | (p2 << 8);
			
			var p3:Int = sock.readByte()&0xff;
			var p4:Int = sock.readByte()&0xff;
			var chunkSizeY:Int = p3 | (p4 << 8);
			
			var q1:Int = sock.readByte()&0xff;
			var q2:Int = sock.readByte()&0xff;
			var screenSizeX:Int = q1 | (q2 << 8);
			
			var q3:Int = sock.readByte()&0xff;
			var q4:Int = sock.readByte()&0xff;
			var screenSizeY:Int = q3 | (q4 << 8);
			
			rc.setScreenData(screenSizeX, screenSizeY, chunkSizeX, chunkSizeY);
			rc.setReader(new ChunkReader(chunkSizeX,chunkSizeY));
			return totalAvailable-2;
		}
		
		return totalAvailable;
	}
}

class ChunkReader implements SocketReader {
	
	private var chunkDataLength:Int;
	
	public function new(chunkX:Int, chunkY:Int) {
		this.chunkDataLength = chunkX * chunkY*4;
	}
	
	private var chunkX:Int;
	private var chunkY:Int;
	private var nextCompressedLength:UInt;
	private var nextIsPadding:Bool=false;
	
	private var state:Int = 0;
	
	public function newDataAvailable(totalAvailable:Int, displayUpdater:DisplayUpdater, sock:Socket, rc:ReaderChanger):Int {
		while(true) {
			if (this.state == 0) { // Receive 5 bytes to coordinates and next size
					if(sock.bytesAvailable >= 5) {
					this.chunkX = sock.readByte() & 0xff;
					this.chunkY = sock.readByte() & 0xff;
					var p1:Int = sock.readByte() & 0xff;
					var p2:Int = sock.readByte() & 0xff;
					this.nextCompressedLength = p1 | (p2 << 8);
					totalAvailable-= 5;
					this.state = 1;
					var a:Int = sock.readByte() & 0xff;
					this.nextIsPadding = a == 1;
				} else {
					return totalAvailable;
				}
			}
			if (this.state == 1) {
				if (sock.bytesAvailable >= this.nextCompressedLength) {
					if (this.nextIsPadding) {
						var chunkData:ByteArray = new ByteArray();
						sock.readBytes(chunkData, 0, this.nextCompressedLength);
						this.state = 0;
						totalAvailable-= this.nextCompressedLength;
					} else {
						var chunkData:ByteArray = new ByteArray();
						sock.readBytes(chunkData, 0, this.nextCompressedLength);
						chunkData.position = 0;
						chunkData.uncompress(CompressionAlgorithm.ZLIB);
						chunkData.position = 0;
						
						var realData:ByteArray = new ByteArray();
						while (chunkData.bytesAvailable != 0){
							var c1 = chunkData.readByte();
							/*
							var c2 = chunkData.readByte();
							var c3 = chunkData.readByte();
							var c4 = chunkData.readByte();
							realData.writeByte(c4);
							realData.writeByte(c3);
							realData.writeByte(c2);
							realData.writeByte(c1);
							*/
							realData.writeByte(c1);
							realData.writeByte(c1);
							realData.writeByte(c1);
							realData.writeByte(c1);
							
						}
						displayUpdater.updateChunk(chunkX, chunkY, realData);
						this.state = 0;
						totalAvailable-= this.nextCompressedLength;
					}
				} else {
					return totalAvailable;
				}
			} 
		}
	}
}